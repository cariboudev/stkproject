﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace STKProject.Models
{
    public class ShareParticipant
    {
        public int Id { get; set; }

        public ICollection<ApplicationUser> Participants { get; set; }

        public ShareParticipant()
        {
            Participants = new Collection<ApplicationUser>();
        }
    }
}