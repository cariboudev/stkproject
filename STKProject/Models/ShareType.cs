﻿using System.ComponentModel.DataAnnotations;

namespace STKProject.Models
{
    public class ShareType
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Type { get; set; }
    }
}