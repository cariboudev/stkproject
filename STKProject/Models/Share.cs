﻿namespace STKProject.Models
{
    public class Share
    {
        public int Id { get; set; }

        public Group Group { get; set; }
        public ApplicationUser Sharer { get; set; }
        public ShareAttachment ShareAttachment { get; set; }
    }
}