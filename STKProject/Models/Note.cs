﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace STKProject.Models
{
    public class Note
    {
        public int Id { get; set; }

        [Required]
        public string Text { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public ICollection<ShareAttachment> ShareAttachments { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public Note()
        {
            ShareAttachments = new Collection<ShareAttachment>();
        }
    }
}