﻿namespace STKProject.Models
{
    public class Attachment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] File { get; set; }
    }
}