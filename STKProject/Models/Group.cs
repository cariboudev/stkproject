﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace STKProject.Models
{
    public class Group
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public ICollection<ApplicationUser> ApplicationUsers { get; set; }

        public Group()
        {
            ApplicationUsers = new Collection<ApplicationUser>();
        }
    }
}