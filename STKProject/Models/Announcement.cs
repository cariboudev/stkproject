﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace STKProject.Models
{
    public class Announcement
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }

        public ICollection<ShareAttachment> ShareAttachments { get; set; }
        public ICollection<ShareParticipant> ShareParticipants { get; set; }

        public Group Group { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public Announcement()
        {
            ShareAttachments = new Collection<ShareAttachment>();
            ShareParticipants = new Collection<ShareParticipant>();
        }
    }
}