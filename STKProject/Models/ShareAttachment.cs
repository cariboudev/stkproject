﻿namespace STKProject.Models
{
    public class ShareAttachment
    {
        public int Id { get; set; }
        
        public ShareType ShareType { get; set; }
        public Attachment Attachment { get; set; }
    }
}