namespace STKProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateAttachmentAndNoteTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MyProperty = c.String(),
                        File = c.Binary(),
                        AttachmentType_Id = c.Int(),
                        Note_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AttachmentTypes", t => t.AttachmentType_Id)
                .ForeignKey("dbo.Notes", t => t.Note_Id)
                .Index(t => t.AttachmentType_Id)
                .Index(t => t.Note_Id);
            
            AddColumn("dbo.Notes", "Start", c => c.DateTime(nullable: false));
            AddColumn("dbo.Notes", "End", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Attachments", "Note_Id", "dbo.Notes");
            DropForeignKey("dbo.Attachments", "AttachmentType_Id", "dbo.AttachmentTypes");
            DropIndex("dbo.Attachments", new[] { "Note_Id" });
            DropIndex("dbo.Attachments", new[] { "AttachmentType_Id" });
            DropColumn("dbo.Notes", "End");
            DropColumn("dbo.Notes", "Start");
            DropTable("dbo.Attachments");
        }
    }
}
