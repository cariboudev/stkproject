namespace STKProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SitePropertyForApplicationUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Site", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Site");
        }
    }
}
