// <auto-generated />
namespace STKProject.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class CreateAttachmentTypeTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateAttachmentTypeTable));
        
        string IMigrationMetadata.Id
        {
            get { return "201706131405228_CreateAttachmentTypeTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
