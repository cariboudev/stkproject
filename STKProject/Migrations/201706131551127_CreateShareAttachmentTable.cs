namespace STKProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateShareAttachmentTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ShareAttachments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                        Attachment_Id = c.Int(),
                        Group_Id = c.Int(),
                        ShareType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .ForeignKey("dbo.Attachments", t => t.Attachment_Id)
                .ForeignKey("dbo.Groups", t => t.Group_Id)
                .ForeignKey("dbo.ShareTypes", t => t.ShareType_Id)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.Attachment_Id)
                .Index(t => t.Group_Id)
                .Index(t => t.ShareType_Id);
            
            DropColumn("dbo.Attachments", "MyProperty");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Attachments", "MyProperty", c => c.String());
            DropForeignKey("dbo.ShareAttachments", "ShareType_Id", "dbo.ShareTypes");
            DropForeignKey("dbo.ShareAttachments", "Group_Id", "dbo.Groups");
            DropForeignKey("dbo.ShareAttachments", "Attachment_Id", "dbo.Attachments");
            DropForeignKey("dbo.ShareAttachments", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ShareAttachments", new[] { "ShareType_Id" });
            DropIndex("dbo.ShareAttachments", new[] { "Group_Id" });
            DropIndex("dbo.ShareAttachments", new[] { "Attachment_Id" });
            DropIndex("dbo.ShareAttachments", new[] { "ApplicationUser_Id" });
            DropTable("dbo.ShareAttachments");
        }
    }
}
