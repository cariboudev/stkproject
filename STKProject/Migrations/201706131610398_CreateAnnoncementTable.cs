namespace STKProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateAnnoncementTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Announcements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreationDate = c.DateTime(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                        Group_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .ForeignKey("dbo.Groups", t => t.Group_Id)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.Group_Id);
            
            AddColumn("dbo.ShareAttachments", "Announcement_Id", c => c.Int());
            CreateIndex("dbo.ShareAttachments", "Announcement_Id");
            AddForeignKey("dbo.ShareAttachments", "Announcement_Id", "dbo.Announcements", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ShareAttachments", "Announcement_Id", "dbo.Announcements");
            DropForeignKey("dbo.Announcements", "Group_Id", "dbo.Groups");
            DropForeignKey("dbo.Announcements", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ShareAttachments", new[] { "Announcement_Id" });
            DropIndex("dbo.Announcements", new[] { "Group_Id" });
            DropIndex("dbo.Announcements", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.ShareAttachments", "Announcement_Id");
            DropTable("dbo.Announcements");
        }
    }
}
