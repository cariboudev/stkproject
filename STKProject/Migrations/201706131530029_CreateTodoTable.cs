namespace STKProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTodoTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Todoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            AddColumn("dbo.Attachments", "Todo_Id", c => c.Int());
            CreateIndex("dbo.Attachments", "Todo_Id");
            AddForeignKey("dbo.Attachments", "Todo_Id", "dbo.Todoes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Attachments", "Todo_Id", "dbo.Todoes");
            DropForeignKey("dbo.Todoes", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Todoes", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.Attachments", new[] { "Todo_Id" });
            DropColumn("dbo.Attachments", "Todo_Id");
            DropTable("dbo.Todoes");
        }
    }
}
