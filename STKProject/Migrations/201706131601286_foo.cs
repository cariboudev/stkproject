namespace STKProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class foo : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ShareAttachments", "Start");
            DropColumn("dbo.ShareAttachments", "End");
            DropColumn("dbo.ShareAttachments", "CreationDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ShareAttachments", "CreationDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ShareAttachments", "End", c => c.DateTime(nullable: false));
            AddColumn("dbo.ShareAttachments", "Start", c => c.DateTime(nullable: false));
        }
    }
}
