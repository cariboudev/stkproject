namespace STKProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BigBang : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ShareAttachments", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Attachments", "AttachmentType_Id", "dbo.AttachmentTypes");
            DropForeignKey("dbo.ShareAttachments", "Group_Id", "dbo.Groups");
            DropForeignKey("dbo.Attachments", "Note_Id", "dbo.Notes");
            DropForeignKey("dbo.Attachments", "Todo_Id", "dbo.Todoes");
            DropIndex("dbo.ShareAttachments", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.ShareAttachments", new[] { "Group_Id" });
            DropIndex("dbo.Attachments", new[] { "AttachmentType_Id" });
            DropIndex("dbo.Attachments", new[] { "Note_Id" });
            DropIndex("dbo.Attachments", new[] { "Todo_Id" });
            CreateTable(
                "dbo.ShareParticipants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Announcement_Id = c.Int(),
                        Event_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Announcements", t => t.Announcement_Id)
                .ForeignKey("dbo.Events", t => t.Event_Id)
                .Index(t => t.Announcement_Id)
                .Index(t => t.Event_Id);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreationDate = c.DateTime(nullable: false),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                        Group_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .ForeignKey("dbo.Groups", t => t.Group_Id)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.Group_Id);
            
            CreateTable(
                "dbo.Shares",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Group_Id = c.Int(),
                        ShareAttachment_Id = c.Int(),
                        Sharer_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.Group_Id)
                .ForeignKey("dbo.ShareAttachments", t => t.ShareAttachment_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Sharer_Id)
                .Index(t => t.Group_Id)
                .Index(t => t.ShareAttachment_Id)
                .Index(t => t.Sharer_Id);
            
            AddColumn("dbo.AspNetUsers", "ShareParticipant_Id", c => c.Int());
            AddColumn("dbo.ShareAttachments", "Event_Id", c => c.Int());
            AddColumn("dbo.ShareAttachments", "Note_Id", c => c.Int());
            AddColumn("dbo.ShareAttachments", "Todo_Id", c => c.Int());
            AddColumn("dbo.Attachments", "Name", c => c.String());
            CreateIndex("dbo.AspNetUsers", "ShareParticipant_Id");
            CreateIndex("dbo.ShareAttachments", "Event_Id");
            CreateIndex("dbo.ShareAttachments", "Note_Id");
            CreateIndex("dbo.ShareAttachments", "Todo_Id");
            AddForeignKey("dbo.AspNetUsers", "ShareParticipant_Id", "dbo.ShareParticipants", "Id");
            AddForeignKey("dbo.ShareAttachments", "Event_Id", "dbo.Events", "Id");
            AddForeignKey("dbo.ShareAttachments", "Note_Id", "dbo.Notes", "Id");
            AddForeignKey("dbo.ShareAttachments", "Todo_Id", "dbo.Todoes", "Id");
            DropColumn("dbo.ShareAttachments", "ApplicationUser_Id");
            DropColumn("dbo.ShareAttachments", "Group_Id");
            DropColumn("dbo.Attachments", "AttachmentType_Id");
            DropColumn("dbo.Attachments", "Note_Id");
            DropColumn("dbo.Attachments", "Todo_Id");
            DropTable("dbo.AttachmentTypes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.AttachmentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Attachments", "Todo_Id", c => c.Int());
            AddColumn("dbo.Attachments", "Note_Id", c => c.Int());
            AddColumn("dbo.Attachments", "AttachmentType_Id", c => c.Int());
            AddColumn("dbo.ShareAttachments", "Group_Id", c => c.Int());
            AddColumn("dbo.ShareAttachments", "ApplicationUser_Id", c => c.String(maxLength: 128));
            DropForeignKey("dbo.ShareAttachments", "Todo_Id", "dbo.Todoes");
            DropForeignKey("dbo.Shares", "Sharer_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Shares", "ShareAttachment_Id", "dbo.ShareAttachments");
            DropForeignKey("dbo.Shares", "Group_Id", "dbo.Groups");
            DropForeignKey("dbo.ShareAttachments", "Note_Id", "dbo.Notes");
            DropForeignKey("dbo.ShareParticipants", "Event_Id", "dbo.Events");
            DropForeignKey("dbo.ShareAttachments", "Event_Id", "dbo.Events");
            DropForeignKey("dbo.Events", "Group_Id", "dbo.Groups");
            DropForeignKey("dbo.Events", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ShareParticipants", "Announcement_Id", "dbo.Announcements");
            DropForeignKey("dbo.AspNetUsers", "ShareParticipant_Id", "dbo.ShareParticipants");
            DropIndex("dbo.Shares", new[] { "Sharer_Id" });
            DropIndex("dbo.Shares", new[] { "ShareAttachment_Id" });
            DropIndex("dbo.Shares", new[] { "Group_Id" });
            DropIndex("dbo.Events", new[] { "Group_Id" });
            DropIndex("dbo.Events", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.ShareParticipants", new[] { "Event_Id" });
            DropIndex("dbo.ShareParticipants", new[] { "Announcement_Id" });
            DropIndex("dbo.ShareAttachments", new[] { "Todo_Id" });
            DropIndex("dbo.ShareAttachments", new[] { "Note_Id" });
            DropIndex("dbo.ShareAttachments", new[] { "Event_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "ShareParticipant_Id" });
            DropColumn("dbo.Attachments", "Name");
            DropColumn("dbo.ShareAttachments", "Todo_Id");
            DropColumn("dbo.ShareAttachments", "Note_Id");
            DropColumn("dbo.ShareAttachments", "Event_Id");
            DropColumn("dbo.AspNetUsers", "ShareParticipant_Id");
            DropTable("dbo.Shares");
            DropTable("dbo.Events");
            DropTable("dbo.ShareParticipants");
            CreateIndex("dbo.Attachments", "Todo_Id");
            CreateIndex("dbo.Attachments", "Note_Id");
            CreateIndex("dbo.Attachments", "AttachmentType_Id");
            CreateIndex("dbo.ShareAttachments", "Group_Id");
            CreateIndex("dbo.ShareAttachments", "ApplicationUser_Id");
            AddForeignKey("dbo.Attachments", "Todo_Id", "dbo.Todoes", "Id");
            AddForeignKey("dbo.Attachments", "Note_Id", "dbo.Notes", "Id");
            AddForeignKey("dbo.ShareAttachments", "Group_Id", "dbo.Groups", "Id");
            AddForeignKey("dbo.Attachments", "AttachmentType_Id", "dbo.AttachmentTypes", "Id");
            AddForeignKey("dbo.ShareAttachments", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
