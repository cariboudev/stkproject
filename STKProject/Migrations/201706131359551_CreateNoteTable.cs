namespace STKProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateNoteTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notes", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Notes", new[] { "ApplicationUser_Id" });
            DropTable("dbo.Notes");
        }
    }
}
