﻿using System.Web.Optimization;

namespace STKProject
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.10.2.min.js",
                        "~/Scripts/js/plugins/jquery/jquery-ui.min.js",
                        "~/Scripts/jquery.unobtrusive-ajax.min.js",
                        "~/Scripts/js/plugins/bootstrap/bootstrap.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/pagePlugins").Include(
                        "~/Scripts/js/plugins/icheck/icheck.min.js",
                        "~/Scripts/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js",

                        "~/Scripts/js/plugins/blueimp/jquery.blueimp-gallery.min.js",
                        "~/Scripts/plugins/dropzone/dropzone.min.js",
                        "~/Scripts/js/plugins/icheck/icheck.min.js",
                        "~/Scripts/js/plugins/bootstrap/bootstrap-datepicker.js",
                        "~/Scripts/js/plugins/fullcalendar/lib/moment.min.js",
                        "~/Scripts/js/plugins/bootstrap/bootstrap-select.js",

                        
                        "~/Scripts/js/plugins/fullcalendar/fullcalendar.min.js",
                        "~/Scripts/js/plugins/fullcalendar/locale/tr.js",

                        "~/Scripts/js/plugins/bootstrap/bootstrap-datepicker.js",
                        "~/Scripts/js/plugins/bootstrap/bootstrap-file-input.js",
                        "~/Scripts/js/plugins/bootstrap/bootstrap-select.js",
                        "~/Scripts/js/plugins/tagsinput/jquery.tagsinput.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/templateJs").Include(
                        //"~/Scripts/js/settings.js",

                        "~/Scripts/js/plugins.js",
                        "~/Scripts/js/actions.js",
                        "~/Scripts/js/demo_maps.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));



            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/css/app").Include(
                       "~/Content/css/theme-default.css"));
        }
    }
}
